<?php


$appId = getenv("APP_ID");

$instanceId = rand();
$instancePath = "/tmp/id";
if(file_exists($instancePath))
    $instanceId = file_get_contents($instancePath);
else
    file_put_contents($instancePath, $instanceId);

echo "This is application: $appId, instance: $instanceId";
